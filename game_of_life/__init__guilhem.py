import numpy as np
import random as rd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from tkinter import *
### créer le tableau et amorcer le jeu
def generate_universe(size):
    #size is a two element-list
    Tableau=np.zeros((size[0],size[1]))
    return Tableau

# def initialisation(n,p,tableau_positions_initiales):
#     Tableau=creertableau(n,p)
#     for i in tableau_positions_initiales:
#         Tableau[i]=1
#     return Tableau

def generate_rd_seed(size_seed):
    #size_seed [nb de colonnes, nb de lignes ]
    # size seed is a two element-list
    seed=np.zeros(size_seed)
    for i in range (size_seed[0]):
        for j in range (size_seed[1]):
            seed[i][j]=rd.randint(0,1)
    return seed

r_pantomino=[[0,1,1],[1,1,0],[0,1,0]]

def place_rd_rdseed(size,size_seed):
    #placement aléatoire d'une graine aléatoire
    #size_seed et size sont de la forme [nb de colonnes, nb de lignes]
    Tableau=generate_universe(size)
    seed=generate_rd_seed(size_seed)
    if size_seed[0]>size[0] or size_seed[1]>size[1]:
        return "tailles incompatibles"
    x_seed=rd.randint(0,size[0]-size_seed[0])
    # x concerne donc les colonnes et y concerne les lignes du tableau
    y_seed=rd.randint(0,size[1]-size_seed[1])
    for i in range(x_seed,x_seed+size_seed[0]):
        for j in range (y_seed,y_seed+size_seed[1]):
            Tableau[i][j]=seed[i-y_seed][j-x_seed]
    return Tableau

def place_seed(size,seed,x_seed,y_seed):
    #size is a two-element list
    #seed is an array
    #x and y are integers
    Tableau=generate_universe(size)
    if x_seed+len(seed[0])>size[0] or y_seed+len(seed)>size[1]:
        return "la graine est mal placée"
    if len(seed[0])>size[0] or len(seed)>size[1]:
        return "tailles incompatibles"
    for i in range(x_seed,x_seed+len(seed[0])):
        for j in range (y_seed,y_seed+len(seed)):
            Tableau[i][j]=seed[i-y_seed][j-x_seed]
    return Tableau

def place_rd_seed(size,seed):
    #placement aléatoire d'une graine connue
    Tableau=generate_universe(size)
    size_seed=[len(seed[0]),len(seed)]
    if size_seed[0]>size[0] or size_seed[1]>size[1]:
        return "tailles incompatibles"
    x_seed=rd.randint(0,size[0]-size_seed[0])
    y_seed=rd.randint(0,size[1]-size_seed[1])
    for i in range(x_seed,x_seed+size_seed[0]):
        for j in range (y_seed,y_seed+size_seed[1]):
            Tableau[i][j]=seed[i-y_seed][j-x_seed]
    return Tableau
### affichage couleur
def display_rdTableau(size,size_seed):
    # size_seed and size are two element-lists
    #ici on connait juste les tailles du tableaiu et de la graine, tout le reste est aléatoire
    Tableau=place_rd_rdseed(size,size_seed)
    for i in range (size[0]):
        for j in range (size[1]):
            if Tableau[i][j]==0:
                Tableau[i][j]=255
            if Tableau[i][j]==1:
                Tableau[i][j]=0
    plt.imshow(Tableau,cmap=cm.jet)
    plt.show()
    return Tableau
###on adapte le programme du dessus pour une graine connue (prévision étape 3) placée aléatoirement
def display_Tableau(size,seed):
    # on place la graine connue aléatoirement dans le tableau
    Tableau=place_rd_seed(size,size_seed)
    for i in range (size[0]):
        for j in range (size[1]):
            if Tableau[i][j]==0:
                Tableau[i][j]=255
            if Tableau[i][j]==1:
                Tableau[i][j]=0
    plt.imshow(Tableau,cmap=cm.jet)
    plt.show()
    return Tableau

def display_tableau2(Tableau):
    for y in range (len(Tableau)):
        for x in range (len(Tableau[0])):
            if Tableau[y][x]==0:
                Tableau[y][x]=255
            if Tableau[y][x]==1:
                Tableau[y][x]=0
    plt.imshow(Tableau,cmap=cm.jet)
    plt.show()

### catalogue d'amorces classiques
seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}
# attention à toujours avoir la taille du tableau >= aux dimensions des graines
### fonctionalité 4
def survival(x_cell,y_cell,Tableau):
    voisins=0
    Tableau_elargi=np.zeros((len(Tableau)+2,len(Tableau[0])+2))
    for i in range(1,len(Tableau)+1):
        Tableau_elargi[i][1:len(Tableau[0])+1]=Tableau[i-1]
    new_x_cell=x_cell+1
    new_y_cell=y_cell+1
    for i in range (new_x_cell-1,new_x_cell+2):
        for j in range(new_y_cell-1,new_y_cell+2):
            if i!= new_x_cell or j!= new_y_cell :
                voisins+=Tableau_elargi[j][i]
    etat_suivant=Tableau[y_cell][x_cell]
    if Tableau[y_cell][x_cell]==1:
        if voisins<2 or voisins>3:
            etat_suivant=0
        else:
            etat_suivant=1
    if Tableau[y_cell][x_cell]==0:
        if voisins==3:
            etat_suivant=1
        else:
            etat_suivant=0
    return etat_suivant

##fonctionalité 5
def generation(Tableau):
    Tableau_suivant=np.zeros((len(Tableau),len(Tableau[0])))
    for x in range (len(Tableau[0])):
        for y in range (len(Tableau)):
            etat_suivant=survival(x,y,Tableau)
            Tableau_suivant[y][x]=etat_suivant
    return Tableau_suivant

##fonctionalité 6
def game_of_life(Tableau,nb_iterations):
    for k in range (nb_iterations):
        Tableau=generation(Tableau)
    return Tableau_suivant
##fonctionnalité 7
#•use plt.pause(500)
# from matplotlib.animation import FuncAnimation
# def init():
#     line.set_data([],[])
#     return line
# def animation_jeu(Tableau,nb_iterations):
#
#     for i in range (nb_iterations):
#         anim=FuncAnimation(Tableau,interval=300)
#         Tableau=generation(Tableau)
#     plt.draw()
#     plt.show
def afficher_jeu(Tableau,n):
    for i in range(n):
        display_tableau2(Tableau)
        plt.pause(0.300)
        plt.clf()
        Tableau=generation(Tableau)
    return Tableau

###Tkinter
from tkinter import *
root=Tk()
champ_label = Label (root, text= "salut !")
champ_label.pack()
root.mainloop()

#un_widget = UnWidget(widget_parent, un_parametre='une_valeur')"
# après création un_widget.config(un_parametre='une_valeur')
##https://vincent.developpez.com/cours-tutoriels/python/tkinter/apprendre-creer-interface-graphique-tkinter-python-3/#LIII-A
# #Label permet d'écrire du texte dans une fenetre crée précedemment
# #pack encapsule le label dans la fenetre
# liste=Listbox(fenetre)
# liste.insert(END, "pierre")
# liste.insert(END, "feuille")
# liste.insert(END, "ciseaux")
# liste.pack()
# fenetre.mainloop()
