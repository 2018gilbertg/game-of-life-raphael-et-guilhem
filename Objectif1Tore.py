import numpy as np
import random as rd

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation


# FONCTIONNALITE 1

def generate_universe(size):                    # size = [number of column, number of row]
    return np.zeros((size[1],size[0]))


def generate_random_seed(size_seed):            # size_seed = [number of column, number of row]
    seed = np.zeros((size_seed[1],size_seed[0]))
    for i in range(size_seed[1]):
        for k in range(size_seed[0]):
            seed[i][k] = rd.randint(0,1)
    return seed


def place_seed(size,seed,x_seed,y_seed):         # size is a 2 element list, seed is an array
    size_seed = [len(seed[0]),len(seed)]
    Tableau = generate_universe(size)
    x_seed = x_seed%size[0]
    y_seed = y_seed%size[1]
    if size_seed[0] > size[0] or size_seed[1] > size[1] :
        return False
    for i in range(y_seed , y_seed + size_seed[1]):
        for k in range(x_seed , x_seed + size_seed[0]):
            if i >= size[1]:
                i -= size[1]
            if k >= size[0]:
                k -= size[0]
            Tableau[i][k] = seed[i - y_seed][k - x_seed]
    return Tableau

def place_random_seed(size,size_seed):
    seed = generate_random_seed(size_seed)
    x_seed = rd.randint(0, size[0] - size_seed[0])
    y_seed = rd.randint(0, size[1] - size_seed[1])
    return place_seed(size,seed,x_seed,y_seed)

#print(place_random_seed([2,4],[1,2]))

r_pentomino = [[0, 1, 1], [1, 1, 0], [0, 1, 0]]

def place_r_pentomino(size,x_seed,y_seed):
    return place_seed(size,r_pentomino,x_seed,y_seed)


# FONTIONNALITE 2

def display(Tableau):
    img = np.zeros((len(Tableau),len(Tableau[0])))
    for i in range(len(Tableau)):
        for j in range(len(Tableau[0])):
            if Tableau[i][j] == 0 :
               img[i][j] = 0
            else :
                img[i][j] = 1
    imgplot = plt.imshow(img,cmap='Greys')
    plt.axis('off')
    plt.show()

#display([[0,0,0,0,0,0],[0,0,1,1,0,0],[0,1,1,0,0,0],[0,0,1,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]])


# FONTIONNALITE 3

seeds = {
    "boat": [[1, 1, 0], [1, 0, 1], [0, 1, 0]],
    "r_pentomino": [[0, 1, 1], [1, 1, 0], [0, 1, 0]],
    "beacon": [[1, 1, 0, 0], [1, 1, 0, 0], [0, 0, 1, 1], [0, 0, 1, 1]],
    "acorn": [[0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0], [1, 1, 0, 0, 1, 1, 1]],
    "block_switch_engine": [
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 0, 1, 0, 1, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0, 0],
    ],
    "infinite": [
        [1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 1],
    ],
}


# FONCTIONNALITE 4

def survival(x_cell,y_cell,Tableau):
    sum_neighbour = 0
    for i in range(x_cell-1,x_cell+2):
        for k in range(y_cell-1,y_cell+2):
            if i != x_cell and k != y_cell:
                i = i%len(Tableau[0])
                k = k%len(Tableau)
                sum_neighbour += Tableau[k][i]
    if Tableau[y_cell][x_cell] == 0:
        if sum_neighbour == 3:
            state_next_step = 1
        else:
            state_next_step = Tableau[y_cell][x_cell]
    if Tableau[y_cell][x_cell] == 1:
        if not 2 <= sum_neighbour <= 3:
            state_next_step = 0
        else:
            state_next_step = Tableau[y_cell][x_cell]
    return state_next_step


# FONCTIONNALITE 5

def generation(Tableau):
    Tableau_next_step = np.zeros((len(Tableau),len(Tableau[0])))
    for x in range(len(Tableau[0])):
        for  y in range(len(Tableau)):
            Tableau_next_step[y][x] = survival(x,y,Tableau)
    return Tableau_next_step

# FONCTIONNALITE 6

def game_life_simulate(Tableau,iterations):
    for i in range(iterations):
        Tableau = generation(Tableau)
    return Tableau

# FONCTIONNALITE 7

#def anim_game_of_life(Tableau):
#    fig = plt.figure()
#    ax = plt.axes(xlim=(0,len(Tableau[0])), ylim=(0, len(Tableau)))
#    line, = ax.plot([], [], lw=2)

#    def init():
#        line.set_data([], [])
#        return line,

#    def display_iteration(i):
#        img = np.zeros((len(game_life_simulate(Tableau,i))),len(game_life_simulate(Tableau,i))[0])))
#        for i in range(len(game_life_simulate(Tableau,i)))):
#            for j in range(len(game_life_simulate(Tableau,i))[0])):
#                if game_life_simulate(Tableau,i))[i][j] == 0 :
#                    img[i][j] = 230
#                else :
#                    img[i][j] = 10
#        line.set_data(x, y)
#        return line,

#    anim = animation.FuncAnimation(fig, display_iteration, init_func=init,frames=200, interval=20, blit=Tr )


def animate_game_of_life(Tableau,iterations):
    for i in range(1,iterations+1):
        img = np.zeros((len(Tableau),len(Tableau[0])))
        for i in range(len(Tableau)):
            for j in range(len(Tableau[0])):
                if Tableau[i][j] == 0 :
                    img[i][j] = 0
                else :
                    img[i][j] = 1
        imgplot = plt.imshow(img,cmap='Greys')
        plt.axis('off')
        plt.pause(0.1)
        Tableau = generation(Tableau)
    plt.show()

#animate_game_of_life([[1,1,0,0],[1,1,0,0],[0,0,1,1],[0,0,1,1]],50)


# FONCTIONNALITE 8

def animate_all_game_of_life(universe_size,seed,seed_position,n_generations,cmap='Greys',interval = 0.1,save=False):
    """
    :param  tuple (int, int) universe_size: dimensions of the universe
    :param seed: (list of lists, np.ndarray) initial starting array
    :param seed_position: (tuple (int, int)) coordinates where the top-left corner of the seed array should be pinned
    :param cmap: (str) the matplotlib cmap that should be used
    :param n_generations: (int) number of universe iterations, defaults to 30
    :param interval: (int )time interval between updates (milliseconds), defaults to 300ms
    :param save: (bool) whether the animation should be saved, defaults to False
    """

    Tableau = place_seed([universe_size[0],universe_size[1]],seed,seed_position[0],seed_position[1])
    for i in range(1,n_generations+1):
        img = np.zeros((len(Tableau),len(Tableau[0])))
        for i in range(len(Tableau)):
            for j in range(len(Tableau[0])):
                if Tableau[i][j] == 0 :
                    img[i][j] = 0
                else :
                    img[i][j] = 1
        imgplot = plt.imshow(img,cmap)
        plt.axis('off')
        plt.pause(interval)
        Tableau = generation(Tableau)
    plt.show()

def animation_doesnt_work(universe_size,seed,seed_position,cmap,n_generations = 30,interval = 0.3,save = False):

    """
    :param  tuple (int, int) universe_size: dimensions of the universe
    :param seed: (list of lists, np.ndarray) initial starting array
    :param seed_position: (tuple (int, int)) coordinates where the top-left corner of the seed array should be pinned
    :param cmap: (str) the matplotlib cmap that should be used
    :param n_generations: (int) number of universe iterations, defaults to 30
    :param interval: (int )time interval between updates (milliseconds), defaults to 300ms
    :param save: (bool) whether the animation should be saved, defaults to False
    """

    Tableau = place_seed([universe_size[0],universe_size[1]],seed,seed_position[0],seed_position[1])

    fig = plt.figure()
    imgplot = plt.imshow(fig,cmap='Greys')
    matrix = plt.plot([[]],[[]])
    plt.xlim(0,universe_size[0])
    plt.ylim(0,universe_size[1])

    def init():
        matrix.set_data([[]],[[]])
        return matrix

    def animate(i):
        Tableau = generation(Tableau)
        imgplot.set_array(Tableau)
        return imgplot

    ani = animation.FuncAnimation(fig, animate, init_func=init, frames=n_generations, blit=True,interval= interval, repeat=False)

    plt.show()
# animation_doesnt_work((10,10),[[1,1,0,0],[1,1,0,0],[0,0,1,1],[0,0,1,1]],(2,2),'Greys')

# FONCTIONNALITE  9

