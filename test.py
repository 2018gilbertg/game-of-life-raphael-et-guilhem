from Objectif1 import *
from pytest import *

def test():
    assert np.all(generate_universe([2,2])==[[0,0],[0,0]])


#def test_create_seed():
#    seed=create_seed(type_seed = "r_pentomino")
#    assert seed==[[0, 1, 1], [1, 1, 0], [0, 1, 0]]

def test_survival():
    assert survival(1,1,[[0,0,0],[0,0,0],[0,0,0]]) == 0
    assert survival(0,1,[[0,0,1],[1,1,0],[1,1,0]]) == 1
    assert survival(1,1,[[1,1,1],[0,0,0],[0,0,0]]) == 1
    assert survival(0,0,[[0,1,0],[1,1,0],[0,0,0]]) == 1

def test_generation():
    assert generation([[0,0,0],[0,0,0],[0,0,0]]) == [[0,0,0],[0,0,0],[0,0,0]]
    assert generation([[0,1,1],[0,1,0],[1,1,0]]) == [[0,1,1],[0,0,0],[1,1,0]]

def test_game_life_simulate():
    assert np.all(game_life_simulate([[0,0,0],[0,0,0],[0,0,0]],6) == [[0,0,0],[0,0,0],[0,0,0]])
    assert np.all(game_life_simulate([[0,1,1],[0,1,0],[1,1,0]],2) == [[0,0,0],[1,0,1],[0,0,0]])
