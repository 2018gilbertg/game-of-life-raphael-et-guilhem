from tkinter import *
from Objectif1SansTore import *
import time



def display_and_update_graphical_gameoflife(iterations,Tableau,interval=0.1):
    size = [len(Tableau[0]),len(Tableau)]
    gameoflife = Tk()
    gameoflife.grid()
    canvas = Canvas(gameoflife,height=600+20,width=600+20)
    canvas.grid()
    canvas.pack()
    for t in range(iterations):
        for i in range(size[0]):
            for j in range(size[1]):
                if Tableau[j][i] == 1:
                    canvas.create_rectangle((i*600/size[0]+10,j*600/size[1]+10), ((i+1)*600/size[0]+10,(j+1)*600/size[1]+10), fill = 'black')
                else:
                    canvas.create_rectangle((i*600/size[0]+10,j*600/size[1]+10), ((i+1)*600/size[0]+10,(j+1)*600/size[1]+10),fill='white')
        gameoflife.update()
        time.sleep(interval)
        Tableau = generation(Tableau)
    canvas.mainloop()


# Tableau=[[0,0,0,0,0,0],[0,1, 1, 0, 0,0], [0,1, 1, 0, 0,0], [0, 0,0, 1, 1,0], [0,0, 0, 1, 1,0],[0,0,0,0,0,0]]
# display_and_update_graphical_gameoflife(30,Tableau)
